# Field Work

## 第一周作业

* 斗地主案例
* 统计一串字符串，求每个字符出现的次数。

🚀🚀作业的源码在文件夹里~

## 第二周作业——18道SQL语句

~~~sql
-- 1．列出至少有一个员工的所有部门。
-- 子查询，在emp表中查出员工大于等于1的部门ID，再去dept表中匹配ID
SELECT DNAME FROM dept WHERE DEPTNO IN ( SELECT DEPTNO FROM emp GROUP BY DEPTNO HAVING COUNT( EMPNO ) >= 1 ) 
-- 2．列出薪金比“SMITH”多或者相等的所有员工。
-- 子查询，先查出SMITH的工资。
SELECT DISTINCT ENAME FROM emp WHERE SAL >= (SELECT SAL FROM emp WHERE ENAME = 'SMITH')
-- 3．列出所有员工的姓名及其直接上级的姓名。
-- 子查询，select后面接两个字段，员工姓名与上级姓名，上级姓名用子查询
SELECT ENAME, ( SELECT ENAME FROM emp AS e2 WHERE e1.MGR = e2.EMPNO ) 'Manager' FROM emp AS e1
-- 4．列出受雇日期早于其直接上级的所有员工。
-- 子查询，查询条件套一个子查询查出上级的受雇日期
SELECT * FROM emp AS e1 WHERE HIREDATE < (SELECT HIREDATE FROM emp AS e2 WHERE e1.MGR = e2.EMPNO)
-- 5．列出部门名称和这些部门的员工信息，同时列出那些没有员工的部门
-- 连接查询，有些部门没员工，所以dept左连接emp，保留所有的dept表信息
SELECT dept.DNAME, emp.ENAME, emp.JOB, emp.MGR, emp.HIREDATE, emp.SAL FROM dept LEFT JOIN emp ON dept.DEPTNO = emp.DEPTNO ORDER BY dept.DEPTNO
-- 6．列出所有“CLERK”（办事员）的姓名及其部门名称。
-- 子查询，查两个字段，第一个字段先查job是clerk的员工信息，第二个字段子查询，匹配部门
-- 或者用连接查询也可以
SELECT e1.ENAME, ( SELECT e2.DNAME FROM dept AS e2 WHERE e1.DEPTNO = e2.DEPTNO ) 'DNAME' FROM emp AS e1 WHERE JOB = 'CLERK'
-- 7．列出最低薪金大于1500的各种工作。
-- 聚合查询，聚合JOB字段，条件是SAL>1500
SELECT JOB FROM emp GROUP BY JOB HAVING MAX( SAL ) > 1500
-- 8．列出在部门“SALES”（销售部）工作的员工的姓名，假定不知道销售部的部门编号。
-- 子查询，先查销售部的ID，再去emp表里匹配
SELECT ENAME FROM emp WHERE DEPTNO = (SELECT DEPTNO FROM dept WHERE DNAME = 'SALES')
-- 9．列出薪金高于公司平均薪金的所有员工。
-- 子查询查出平均值，在查高于平均值的员工
SELECT ENAME FROM emp WHERE SAL > (SELECT AVG( SAL ) FROM emp)
-- 10．列出与“SCOTT”从事相同工作的所有员工。
-- 子查询先查SCOTT的工作，最后加一个不等于条件去掉SCOTT自己
SELECT ENAME FROM emp WHERE JOB = ( SELECT JOB FROM emp WHERE ENAME = 'SCOTT' ) AND ENAME != 'SCOTT'
-- 11．列出薪金等于部门30中员工的薪金的所有员工的姓名和薪金。
-- 子查询加条件查询，先查部门30的薪金，返回的应该是一个薪金集合
SELECT ENAME, SAL FROM emp WHERE SAL IN (SELECT SAL FROM emp WHERE DEPTNO = 30)
-- 12．列出薪金高于在部门30工作的所有员工的薪金的员工姓名和薪金。
-- 子查询，先查部门30的最高工资
SELECT ENAME, SAL FROM emp WHERE SAL > (SELECT MAX( SAL ) FROM emp WHERE DEPTNO = 30)
-- 13．列出在每个部门工作的部门名称,员工数量、平均工资。
-- 子查询加聚合函数，不过有一个部门没有员工，这里没有处理
SELECT ( SELECT dept.DNAME FROM dept WHERE dept.DEPTNO = emp.DEPTNO ) 'DNAME', COUNT( DEPTNO ), AVG( SAL ) FROM emp GROUP BY DEPTNO
-- 14．列出所有员工的姓名、部门名称和工资(含奖金)。
-- 连接查询+ifnull函数处理null值
SELECT emp.ENAME, dept.DNAME, (emp.SAL + IFNULL( emp.COMM, 0 )) '工资+奖金' FROM emp LEFT JOIN dept ON emp.DEPTNO = dept.DEPTNO
-- 15．列出所有部门的详细信息和部门人数。
-- 子查询，在dept表后用子查询添加一个部门人数字段，用聚合函数
SELECT e1.*, ( SELECT COUNT( DEPTNO ) FROM emp AS e2 WHERE e1.DEPTNO = e2.DEPTNO GROUP BY DEPTNO ) '部门人数' FROM dept AS e1
-- 16．列出各种工作的最低工资。
-- 聚合函数
SELECT JOB, MIN( SAL ) FROM emp GROUP BY JOB
-- 17．列出各个MANAGER（经理）的最低薪金。
-- 先挑选出JOB为经理的，最后聚合统计最小工资
SELECT ENAME, MIN( SAL ) FROM emp WHERE JOB = 'MANAGER' GROUP BY DEPTNO
-- 18．列出所有员工的年工资,按年薪从低到高排序。
-- 聚合函数，以及对null的处理
SELECT ENAME, (SAL + IFNULL( COMM, 0 )) * 12 AS '工资+年薪' FROM emp ORDER BY ( SAL + IFNULL( COMM, 0 )) * 12
~~~

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/Guo-Hanzhe/field-work.git
git branch -M main
git push -uf origin main
```

