package demo02;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

//  键入一串字符串，求每个字符出现的次数
public class Solution {

//    输出字符串中每个字符出现的次数，忽略大小写
    public static void sumOfCharacters(String str) {
        Map<Character, Integer> map = new HashMap<>();
        String tempStr = str.toLowerCase();
        for (int i = 0; i < tempStr.length(); i++) {
            int count = map.getOrDefault(tempStr.charAt(i), 0);
            map.put(tempStr.charAt(i), ++count);
        }
//        输出结果
        System.out.println("不区分大小写的情况下：");
        map.forEach((key, value) -> {
            System.out.println(key + "出现了：" + value + "次。");
        });
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str = sc.next();
        sumOfCharacters(str);
    }
}
