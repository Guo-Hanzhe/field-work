package demo;

import java.util.*;

public class Demo {

//    模拟斗地主洗牌发牌的小案例：集合使用
    public static void main(String[] args) {
//        将牌库随机分配给3个人+底牌
//        准备牌库
        Map<Integer, String> pokers = new HashMap<>();
        String[] types = new String[]{"♠", "♥", "♣", "♦"};
        String[] numbers = new String[]{"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"};
        List<Integer> nos = new ArrayList<>();
        int index = 0;
        for (String number : numbers) {
            for (String type : types) {
                pokers.put(index, type + number);
                nos.add(index);
                index++;
            }
        }
        nos.add(52);
        pokers.put(52, "小🃏");
        nos.add(53);
        pokers.put(53, "大🃏");
//        模拟洗牌，乱序方法
        Collections.shuffle(nos);
//        模拟发牌
        List<Integer> playerNo1 = new ArrayList<>();
        List<Integer> playerNo2 = new ArrayList<>();
        List<Integer> playerNo3 = new ArrayList<>();
        List<Integer> playerNo4 = new ArrayList<>();
        for (int i = 0; i < nos.size(); i++) {
            if (i < 17) {
                playerNo1.add(nos.get(i));
            }else if (i < 34) {
                playerNo2.add(nos.get(i));
            } else if (i < 51) {
                playerNo3.add(nos.get(i));
            } else {
                playerNo4.add(nos.get(i));
            }
        }
//        整理手牌（对牌号排序）
        Collections.sort(playerNo1);
        Collections.sort(playerNo2);
        Collections.sort(playerNo3);
        Collections.sort(playerNo4);
        List<String> poker1 = new ArrayList<>();
        List<String> poker2 = new ArrayList<>();
        List<String> poker3 = new ArrayList<>();
        List<String> poker4 = new ArrayList<>();
        for (Integer integer : playerNo1) {
            poker1.add(pokers.get(integer));
        }
        for (Integer integer : playerNo2) {
            poker2.add(pokers.get(integer));
        }
        for (Integer integer : playerNo3) {
            poker3.add(pokers.get(integer));
        }
        for (Integer integer : playerNo4) {
            poker4.add(pokers.get(integer));
        }
        System.out.println(poker1);
        System.out.println(poker2);
        System.out.println(poker3);
        System.out.println(poker4);
    }
}
